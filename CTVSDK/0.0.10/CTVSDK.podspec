#
# Be sure to run `pod lib lint CTVSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CTVSDK'
  s.version          = '0.0.10'
  s.summary          = 'ClusterTV iOS SDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
ClusterTV iOS SDK.
                       DESC

  s.homepage         = 'https://bitbucket.org/kinnrot/ios-sdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'antonzy' => 'antonzy90@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/kinnrot/ios-sdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.2'

  s.source_files = 'CTVSDK/Classes/**/*'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }

  # s.resource_bundles = {
  #   'CTVSDK' => ['CTVSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit','Foundation', ''MapKit'
    s.dependency 'ObjectMapper', '~> 2.2'
end
